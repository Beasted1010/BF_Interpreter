

#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define TAPE_LENGTH 50


// Some things I noticed for potential future improvement
// TODO: Use a stack to handle loops instead of my wonky method
// TODO: Actually implement a lexer step, parsing step, perhaps make some sort of AST representation
// TODO: Make things simpler (: Probably could do this with a single for-loop over all characters and a switch statement within



void ValidateObject( void* ptr, char* name )
{
    if( !ptr )
    {
        printf("Failed to allocate memory for %s!\n", name);
    }
}

typedef enum Tokens
{
    MOVE_LEFT,
    MOVE_RIGHT,
    INCREMENT,
    DECREMENT,
    PRINT,
    INPUT,
    LOOP_START,
    LOOP_END,
    NUM_TOKENS,
    INVALID
} Tokens;

char TOKENS[NUM_TOKENS] = { '<', '>', '+', '-', '.', ',', '[', ']' };

typedef struct Pool
{
    char* memory;
    uint32_t size;
} Pool;


Pool CreatePool(uint32_t size)
{
    Pool pool;
        
    pool.memory = calloc(size, sizeof(char));
    ValidateObject(pool.memory, "pool.memory");
    pool.size = size;

    printf("Pool Created...\n");
    return pool;
}

void DestroyPool(Pool* pool)
{
    free(pool->memory);

    printf("Pool Destroyed...\n");
}

typedef struct Tape
{
    char* memory;
    uint32_t length;
    uint32_t location;
} Tape;

Tape CreateTape(uint32_t length)
{
    Tape tape;

    tape.memory = calloc(length, sizeof(char));
    ValidateObject(tape.memory, "tape.memory");

    tape.length = length;
    tape.location = 0;

    printf("Tape Created...\n");
    return tape;
}

void ReadTape(Tape* tape, uint32_t start, uint32_t end)
{
    if(start == end)
    {
        start = 0;
        end = tape->length - 1;
    }

    for(int i = start; i <= end; i++)
    {
        printf("Cell #%i has value: %c\n", i, tape->memory[i]);
    }
}

char GetValueOnTape(Tape* tape)
{
    return tape->memory[tape->location];
}

char ReadValueOnTape(Tape* tape, uint8_t char_interpret_flag)
{
    printf("Tape at location %i has ", tape->location);
    if(char_interpret_flag)
    {
        printf("character value: %c\n", GetValueOnTape(tape));
    }
    else
    {
        printf("integer value: %i\n", GetValueOnTape(tape));
    }
}

void DestroyTape(Tape* tape)
{
    free(tape->memory);

    printf("Tape Destroyed...\n");
}

typedef struct Program
{
    Pool pool;
    Tape tape;
    char* source;
    uint32_t source_length;
    uint32_t pc;
    char* output;
    uint32_t output_length;
} Program;

Program CreateProgram(char* source, uint32_t tape_length)
{
    Program program;

    program.tape = CreateTape(tape_length);

    program.tape.length = tape_length;
    program.tape.location = 0;

    program.pc = 0;
    program.source_length = strlen(source);
    program.source = malloc(sizeof(char) * program.source_length);
    ValidateObject(program.source, "program.source");

    for(int i = 0; i < program.source_length; i++)
    {
        program.source[i] = source[i];
    }

    program.output = NULL;
    program.output_length = 0;

    printf("Program Created...\n");
    return program;
}

void DeleteProgram(Program* program)
{
    DestroyTape( &(program->tape) );
    free(program->source);
    free(program->output);
    printf("Program Destroyed...\n");
}

uint32_t GetLoopCommands(Program* program, char** commands_out)
{
    if(program->source[program->pc] != '[')
    {
        printf("Not at loop start! Terminating retrieval of loop commands!\n");
        return 0;
    }

    uint32_t num_commands = 0;
    uint32_t num_opened_brackets = 1;
    uint32_t loop_pc = program->pc + 1; // Skip the opening loop bracket ('[')

    while(num_opened_brackets > 0)
    {
        if(program->source[loop_pc] == '[')
        {
            num_opened_brackets++;
        }
        else if(program->source[loop_pc] == ']')
        {
            num_opened_brackets--;
        }

        // Only want to consider commands that are for this loop
        *commands_out = realloc( *commands_out, sizeof(char) * (++num_commands) );
        (*commands_out)[num_commands-1] = program->source[loop_pc];

        loop_pc++;
    }

    printf("There are %i commands in loop\n", num_commands);
    printf("The commands are: ");
    for(int i = 0; i < num_commands; i++)
    {
        printf("%c", (*commands_out)[i]);
    } printf("\n");

    return num_commands;
}

void RunLoop(Program* program, char* commands, uint32_t num_commands);

void RunCommand(Program* program)
{
    printf("Current command: %c\n", program->source[program->pc]);

    switch(program->source[program->pc])
    {
        case '+':
        {
            program->tape.memory[program->tape.location]++;
            printf("After value increment... "); ReadValueOnTape(&program->tape, 0);
        } break;

        case '-':
        {
            program->tape.memory[program->tape.location]--;
            printf("After value decrement... "); ReadValueOnTape(&program->tape, 0);
        } break;

        case '<':
        {
            if(program->tape.location - 1 < 0)
            {
                printf("Moving left a cell at location %i will cause an Out of Bounds Error! Cannot comply!\n", program->tape.location);
            }
            else
            {
                program->tape.location--;
                printf("After location decrement... Tape is at location %i\n", program->tape.location);
            }
        } break;

        case '>':
        {
            if(program->tape.location + 1 >= program->tape.length)
            {
                printf("Moving right a cell at location %i will cause an Out of Bounds Error! Cannot comply!\n", program->tape.location);
            }
            else
            {
                program->tape.location++;
                printf("After location increment... Tape is at location %i\n", program->tape.location);
            }
        } break;

        case '[':
        {
            char* commands = NULL;
            uint32_t num_commands = GetLoopCommands(program, &commands);
            RunLoop(program, commands, num_commands);
            free(commands);

            // Loop complete, update program counter, include jumping over the 2 loop brackets ('[' & ']')
            program->pc += num_commands - 1; // Subtract 1 to compensate for additional add below
            printf("Program counter after adding %i = %i\n", num_commands - 1, program->pc);
        } break;

        case ']':
        {

        } break;

        case '.':
        {
            // Want to add one more character our program's output
            program->output = realloc(program->output, sizeof(char) * (program->output_length + sizeof(char)) );
            ValidateObject(program->output, "program->output");

            program->output[program->output_length++] = (char) program->tape.memory[program->tape.location];
        } break;

        case ',':
        {
            printf("Grabbing input character...\n");
            int val;
            scanf("%d", &val);
            printf("Read in %c to tape location %i\n", val, program->tape.location);
            program->tape.memory[program->tape.location] = val;
            getchar(); // Eat extra newline
        } break;

        default:
        {
            printf("Invalid command: %c\tSkipping...\n", program->source[program->pc]);
        } break;
    }

    program->pc++;
    printf("After command, PC = %i\n", program->pc);
}

uint32_t CountInnerLoopCommands(char* commands, uint32_t num_total_commands)
{
    uint8_t num_inner_loop_commands = 0;
    uint8_t num_open_brackets = 1;

    for(int i = 0; i < num_total_commands; i++)
    {
        if(commands[i] == '[')
        {
            num_open_brackets++;
        }
        else if(commands[i] == ']')
        {
            num_open_brackets--;
        }

        if( num_open_brackets > 1 )
        {
            num_inner_loop_commands++;
        }
    }

    printf("Number of inner loop commands = %i\n", num_inner_loop_commands);
    return num_inner_loop_commands;
}

void RunLoop(Program* program, char* commands, uint32_t num_commands)
{
    program->pc++; // Step over the opening loop bracket ('[')

    uint32_t inner_loop_commands = CountInnerLoopCommands(commands, num_commands);
    uint32_t outer_loop_commands = num_commands - inner_loop_commands;

    printf("Entering loop...\n");
    while(program->tape.memory[program->tape.location] != 0)
    {
        ReadValueOnTape(&program->tape, 0);
        for(int i = 0; i < outer_loop_commands; i++)
        {
            printf("Loop is on command #%i\n", i);
            RunCommand(program);
        }
        program->pc -= num_commands; // Return to the start of the loop
        printf("Program counter value after subtracting %i = %i\n", num_commands, program->pc);
    }
    printf("Exiting loop...\n");
}

void RunProgram(Program* program)
{
    while(program->pc < program->source_length)
    {
        RunCommand(program);
        printf("Program counter has value of %i and source length is %i\n", program->pc, program->source_length);
    }
}

void PrintProgramOutput(Program* program)
{
    printf("-------------------------\n");
    printf("The program output is...\n");

    for(int i = 0; i < program->output_length; i++)
    {
        printf("%c", program->output[i]);
    }
    printf("\n");
    printf("-------------------------\n");
}



int main(int* argc, char** argv)
{
    // Debugging test cases
    //char* source = "++++++++++++++++++++++++++++++++++++++++++++++++.+.+.+.+.+.+.+.+.+.-------------------------.>++++++++++++++++++++++++++++++++++++++++++++++++.+.+.+.+.+.+.+.+.+.+";
    //char* source = "+[++-[>++[>++--]<--]<--]";
    //char* source = "+[++-[>++--]<--]";
    //char* source = "+[++---]";
    //char* source = ",.,.,.";
    //char* source = ">+[>,]<[<]>>[.>]";

    // Practical test cases
    char* source = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";
    //char* source = "+++++++++++++++++++++++++++++++++++++++++++++++++++.[->+<]++++++++++++++++++++++++++++++++.>.";
    Program program = CreateProgram(source, TAPE_LENGTH);

    RunProgram(&program);
    PrintProgramOutput(&program);

    DeleteProgram(&program);
}


